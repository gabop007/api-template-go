module github.com/gabomarf/test

go 1.15

require (
	github.com/go-ozzo/ozzo-routing/v2 v2.3.0
	github.com/qiangxue/go-rest-api v1.0.1 // indirect
	go.uber.org/zap v1.16.0 // indirect
)
